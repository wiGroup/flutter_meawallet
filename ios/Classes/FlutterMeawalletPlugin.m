#import "FlutterMeawalletPlugin.h"
#if __has_include(<flutter_meawallet/flutter_meawallet-Swift.h>)
#import <flutter_meawallet/flutter_meawallet-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_meawallet-Swift.h"
#endif

@implementation FlutterMeawalletPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterMeawalletPlugin registerWithRegistrar:registrar];
}
@end
