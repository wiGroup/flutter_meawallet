import 'dart:async';

import 'package:flutter/services.dart';

class FlutterMeawallet {
  static const MethodChannel _channel =
      const MethodChannel('flutter_meawallet');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  /*
 * Check if device supports NFC
 */
  static Future get isNFCAvailable async {
    return await _channel.invokeMethod('isNFCAvailable');
  }

  /*
  * Initialise the mea_wallet sdk
  * firbase messaging token REQUIRED as parameter
  * {
  *   'token': token,
  * };
  */
  static Future initialise(Map<String, String> props) async {
    await _channel.invokeMethod('initialise', props);
  }

  /*
   * Check if current app have benn set as default Tap to Pay app
   * Will prompt user to set as default if it is not set as default
   */

  static void get checkDefaultTapToPayApp async {
    await _channel.invokeMethod('setAsDefaultPayment');
  }

  /*
   * Check if user digitised his card
   */
  static Future get didDigitizeCard async {
    return await _channel.invokeMethod('getDefaultCard');
  }

  /*
   * Digitise user's card
   * 
   * e.g of Params
   * Params
   * var payload = {
      "accountNumber": device.pan,
      "expiryMonth": expiryMonth,
      "expiryYear": expiryYear,
      "cardholderName": cardholderName
    };

    var encodedPayload = json.encode(payload);

    MethodChannel channel = MethodChannel('MEA_CHANNEL');
    Map<String, dynamic> MEAData = {
      'payload': encodedPayload,
      'cvc2': cvc2,
      'pubKeyMod': meaPubKeyMod,
      'publicKeyFingerprint': meaPublicKeyFingerprint,
      'pubKeyExp': meaPubKeyExp
    };
   */
  static Future digitizeCard(Map<String, String> props) async {
    return await _channel.invokeMethod('digitizeCard', props);
  }

  /*
   * Deactivate tap to pay feature for selected card
   */
  static Future get deactivateCard async {
    return await _channel.invokeMethod('deactivateCard');
  }

  /*
   * Do contactless payment
   */
  static Future get doContactlessPayment async {
    return await _channel.invokeMethod('doContactlessPayment');
  }

  /*
   * Cancel contactless payment
   */
  static Future get cancelContactlessPayment async {
    return await _channel.invokeMethod('cancelContactlessPayment');
  }

  /*
   * Prompt user to authenticate himself using pin/biometrics
   */
  static Future get authenthicateUser async {
    return await _channel.invokeMethod('authenthicateUser');
  }

  /*
   * QA ONLY!!
   * Trigger a succesful transaction
   */
  static Future get testRemoteTransaction async {
    return await _channel.invokeMethod('testRemoteTransaction');
  }
}
